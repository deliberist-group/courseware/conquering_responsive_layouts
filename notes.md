# Week 1 - fundamentals!

## Day 1

[Challenge 01-4](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/01-4)

- Use relative units like percentages rather than absolute units like pixels.

- Percentages are relative to the parent of the block element.

- **Websites are naturally responsive**, avoid setting heights and widths unless
  there is a good reason to do so.

  - A more responsive alternative is to set a large padding with the _relative_
    units "em" or "rem".

- Some notes about about em/rem:

  - They basically translate to pixels.  The default font-size in most browsers
    is "16px" so 1em=16px, 2em=32px, etc.

  - The "em" is relative to the font-size of the next closest parent block,
    while "rem" is relative to the root block.


## Day 2

No new lessons or challenges for today.

## Day 3

[Challenge 01-7](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/01-7)

- Relative units help with small screens, but the same "design" (eg width) might
  not look great on larger screens.  Here comes `max-width: X;`

- There is also a `min-width` property, but generally a min-width fights you
  when it comes to responsive designs.

## Day 4

- No videos for today, it's intended to be a catch up day for some.

- But Kevin did mention "_viewport units_" and did talk about them in a previous
  [video here](https://www.youtube.com/watch?v=IWFqGsXxJ1E).  He used viewport
  units for `font-size` in that video but he does caution that using viewport
  units for a font size does cause some accessibility issues.  So although it's
  a cool concept, it's probably not best to use viewport units for font sizes.

- Also, viewport units (eg. vw) work well for headings as they scale nicely
  based on the screen size.  The same ***cannot*** be said for paragraphs.

## Day 5

[Challenge Day 05](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-05)

- I've noticed a few times that Kevin uses two values for things like `margin`
  and `padding`.  For example, he would have `margin: 0 auto;` and what this
  does is apply a 0-px margin to the top/bottom sides while setting the 
  left/right side to automatically fill.  Using `width`/`max-width` and this two
  value `margin` is a great way to horizontally center a something.

- Something to memorize, is widths should use percentages and max-widths should
  use explicit pixel values.

## Day 6

- There wasn't any challenge for today.

- The main topic for today was about the `em`/`rem` units, and how they operate.

- Essentially, **do not use `em` for `font-size` properties.**  This is because
  em's compound from their parent element when used in `font-size`.  When an em
  is not used in a `font-size` then they are referencing that current element's
  font-size.

  - The compounding issue can be seen when trying to use them on a list's font
    size.  [See this video, ~3min marker.](https://www.youtube.com/watch?v=pautqDqa54I)
    This is because nested lists will compound their font-size `em`s.

- The `rem` unit always references the root element (eg `<html>`), and usually
  defaults to 16px.  But this default can theoretically change between browsers
  and user settings.

## Day 7

- [Emmet documentation](https://docs.emmet.io/)

- [BEM](http://getbem.com/naming/)

  - B = Block = things like a div.

  - E = Elements = things inside the block, like h1-6, p, ol, ul, img, button, 
    etc...

  - M = Modifier = some type of _modifier_ to an existing block/element.  For
    example, trying to highlight something, or simply just tweaking a
    block/element.

- [Roboto font](https://fonts.google.com/specimen/Roboto?preview.text=roboto&preview.text_type=custom&sidebar.open&selection.family=Roboto:wght@100;300;400;500;700;900)

- Watching Kevin go through his solution to the Day 5 challenge, it's clear for
  something like this there is always an _extra_ container `<div>` that has
  both a `width` and `max-width` property.  This helps make the container
  responsive on both small and large screen sizes.

# Week 2 - flexbox!

## Day 8

[Challenge 02-3](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/02-3)

- To a flex container, use `display: flex`, which has a default of 
  `flex-direction: row`.

- Flex items try to shrink to the smallest possible width, even on things like
  divs that default to `width: 100%`.

- There's a property called `gap` (and `row-gap` and `column-gap`) is only
  supported in Firefox, as of today (2020-April-21).  Use the site
  [Can I Use](https://caniuse.com/#feat=flexbox-gap) to check HTML5 and CSS
  properties against different browsers and their versions for compatibility.

  - In lieu of `gap`, we can use fancy CSS selectors to add a gap, like such and
    add in a margin or padding.  Add a background color to see what is going on
    since this fancy CSS selector will not select the first element in a
    container of items.

```CSS
.col + .col {
  background-color: limegreen; /* just to see what's going on! */
  margin-left: X;
  padding-left: Y;
}
```

## Day 9

[Challenge 02-6](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/02-6)

- After watching the video on Kevin's Flexbox Challenge 01 solution, it is clear
  that I need to add much more modifications to the markup.  I need to add more
  containers (ie `div` blocks), I need to add more classes that I can select on,
  and I need to be more careful about how I select things that do not have a
  class name on them.  Also something I can do is add multiple classes to a
  single block.

- Flex box tries to make all columns (when `flex-direction: row`) all have the
  same height.  This may or may not be what you want.  To stop this behaviour,
  we can set on the `row` the `align-items: flex-start;` property.

  - Alternatively, if you didn't want to align all the items in a `row`, you can
    set on the individual item the property `align-self: flex-start;`.

- When there is empty space on a `row`, where the items have been explicitly set
  to widths (cumulatively) below 100%, then you can control where flexbox puts
  the empty space with:

  - `justify-content: flex-end;     /* Adds space at the end of the row. */`
  - `justify-content: center;       /* Adds space at the beginning AND end of the row. */`
  - `justify-content: space-evenly; /* ? */`
  - `justify-content: space-around; /* ? */`
  - `justify-content: space-between;/* Only puts space in between items, not on the left of the first nor right of the last item. */`

- I'm using the phrase "_on the row_" by meaning the container that has 
  `display: flex` set.

- Flexbox is fucking weird when things try to shrink and grow as they please!

## Day 10

- Nothing much from today, just a [video that dives deeper into flexbox](https://www.youtube.com/playlist?list=PL4-IK0AVhVjMSb9c06AjRlTpvxL3otpUd).

## Day 11

[Challenge Day 11](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-11)

- Basically Kevin just showed a few standards pertaining to markup.

  - He first showed that a top navigation bar is usually nested in something
    like: `<body> <header> <nav> ... </nav> </header> ... </body>` tags.

- Just something else to note, once we made the nav bar `display: flex;` they
  did not expand the full width of the page.  Flex items try to shrink to the
  smallest possible size, while `div` elements try to full up `width: 100%;` of
  its parent.

## Day 12

[Challenge Day 11](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-11)

- Kevin showed two ways to break up the nav bar where the *Sign In* and *Sign
  up* links are pushed to the right.

  - The first way was to choose the first element in the list that will push all
    the remaining elements to the right, and add `margin-left: auto`.  This
    could be done with an extra class.  Yes, to push to the right the left
    margin needed to be auto'd.

  - The other way was to break up the lists in the nav bar into two separate
    lists.

  - But Kevin did make a point that there should only be one `<nav>...</nav>`
    in a document as it is designed to label the main navigation for a site.

- As per the challenge, I think I got most of the markup completed.  The
  elements are there, they may need some more classes.  The `style.css` still
  needs a bit more work.  I need to center the `h2` for the subheader, add more
  space between the paragraphs in the subjeader, and then alternate 
  row-reversing the main content rows.

## Day 13/14

- Nothing more than an email today, as Kevin wanted us to continue implementing
  the design for the
  [latest challenge from Day 11](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-11).

- He did send us a link to one of his videos talking about
  [min(), max(), and clamp()](https://www.youtube.com/watch?v=U9VF-4euyRo) that
  could be used to more concisely set responsive font sizes.

# Week 3

## Day 15

- No challenge for today, so we have more time to complete the
  [latest challenge from Day 11](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-11).

- There were two videos that talked about ***media queries***.

  - There are two types of media queries, a ***media type*** and a ***media
  feature***.

- The purpose of these is to effectivly override CSS properties based on the
  properties of the device the client is using.

- Generally, people use media query to change properties based on the width of
  the client's screen size.  Such a query would look something like:

  - `@media (min-width: 600px) { ... }` - for screens larger than 600px.

  - `@media (max-width: 600px) { ... }` - for screens smaller than 600px.

  - There are other things we can define our media query on, it does not have to
    only be for min/max widths.

- It is important to remember that the order of the media queries is important.
  Anything _lower (or later) in the CSS_ overrides previous properties.  So if
  there is a media query changing a font size on line 100 and then the normal
  CSS setting the font size on line 200, the normal setting will always override
  the media query because it is set after the media query.

  - Because of this _fun fact_, it is best to put media queries at the bottom of
    all of the CSS.

- Interestingly, in his second video he changed the
  [Challenge 02-6](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/02-6)
  so that is has a media query to turn the columns into rows but he did not
  change the flex properties.  I would have thought he would change the property
  `flex-direction: row;` to `flex-direction: column;` but what he actually did
  was set `display: block`.

  - I have not tested my idea, but I suspect this might be a situation where
    there are multiple solutions.  And Kevin chose the `display: block;` route.

## Day 16

- Today's topic was about breakpoints, meaning the point at which the media
  queries change the style.

- Kevin's response was to look at the layout/design and see when things start to
  break the design.  Then you'll need a breakpoint (media query) to change the
  layout.  You can have as many break points as you want, but for simplicity the
  designer will want to keep this to a minimum.  Using too many break points
  make the design/layout hard to maintain.

- There is not a good, general rule as to where to set break points because the
  layout, font sizes, paddings/margins, and the like all affect the design.  So
  for example, one design might be OK breaking at 600px, but another design
  might make use of larger font sizes so that will need to break at something
  larger like 900px.

- Lastly, Kevin posted a link to an article that describes the
  [100% correct way to do CSS breakpoints](https://www.freecodecamp.org/news/the-100-correct-way-to-do-css-breakpoints-88d6a5ba1862/).

  - The TL;DR of the article is: use 600px, 900px, 1200px, and 1800px if you
    plan on giving the giant-monitor people something special.

## Day 17

- Today was only about the `<meta ...>` tag where it basically just tells the
  browser to be responsive.  Without it, essentially some browsers flat out
  ignore media queries.  For example, Chrome will actually just scale the whole
  webpage down, which will probably make text ungodly small to read.

- The only other take-away from this short lesson is to just copy and paste this
  line into all your HTML documents:

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

## Day 18

- Kevin gave his solution to the 
  [Day 12 challenge](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-12-solutions).

- Kevin said something that sort of hit home.  He said it is fairly common to
  remove margins from headings, or at least margin tops.  This is because the
  headings will usually create extra space on top of stuff for nothing.  This is
  especially true when margins collapse.

- Kevin showed how he styled his site (the solution to the challenge) with
  mobile first in mind.  He was able to get the site nearly perfect with almost
  minimal CSS.  Since HTML without CSS is responsive to begin with, much of what
  he did was just add colors, some space (padding/margin), and some fonts.  The
  default HTML behavior seems to fit the mobile device sizes well.

- He then moved onto modifying the CSS to account for large/desktop screen
  sizes.

  - He made a point that designers will never let a line of text span the full
    width of a large screen.  It does not look good, but more practically it
    does not read well.  That's why designers put text into two or more columns.

  - For a media query like this where we are first coding for small screens then
    using media queries to account for large screens we want our query to be
    `@media (min-width: N px) { ... }`.

  - Kevin mentioned in his experience that doing mobile first then account for
    larger screen sizes seems to group colors and typeography properties 
    together while the media queries tent to group more layout properties
    together.

## Day 19

- The second to last challenge was introduced for today,
  [day 19](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-19).

- The purpose of this challenge was to make the navigation bar hidden behind a
  hamburger click for mobile screens.  Basically use media queries to change the
  nav bar.

- I did not really finish this challenge.

## Day 20

- Fixed the 
  [day 19](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/day-19)
  challenge with Kevin's solution.

## Day 21

- Kevin did a little recap for the final day.

  - Websites are responsive before we write any CSS.

  - When our layouts run into issues, we are the the one's at fault.

  - _Usually (though not always)_ a desktop first approach is the culprit when
    something in our layout does not work for large screen sizes.

  - Writing mobile-first CSS tends to be the way easier approach, even if you
    only have a desktop layout to base things off of.  I think this is because
    stock HTML with no CSS kind of already has a layout appealing for mobile
    device sizes.

  - There is a shit ton more about flexbox!!

  - There is CSS Grid, which Kevin decided not to touch on it since it is
    massive and has way too much to talk about.  He didn't want to put too much
    into the course.

- All in all, Kevin's course was top notch.  He seemed to really value the
  students practicing the material, not just drone on in a bunch of videos.

- There was one final challenge that Kevin may not post the solution to, it is
  really just to test one's abilities.  Changes are I will not attempt the
  challenge, just don't have the time.  He supplied a design doc (PDF) as well
  as this description:

> A few considerations
> I've only provided a desktop version of the design (except for one section).
> Despite this, I want you to take a mobile-first approach.
>
> One of the most common reasons I hear as an excuse for writing desktop-first
> is that's what the design gives them. We've seen why we can still tackle
> things mobile-first despite that, so let's put it into practice!
>
> The black box around the design is the viewport. The content inside should be
> limited to the max-width given in the design specs, but the backgrounds should
> extent to the edges of the viewport no matter the size.
>
> For the fonts, I made a mistake in the design specs. I'll be updating the file
> in the coming days, but until then, you can update any use of Antartician
> Headline with Oswald, which is a Google font. You may have to adjust the
> font-sizes a little since it's a different font.  Sorry about that!
>
> The PDF outlines 2 specific breakpoints to use. You can modify them a little
> if you feel that it's appropriate, but you should aim for those ballparks.
>
> Don't feel any shame in going back over previous lessons if you get stuck. The
> only rule is if you do have to go back, do not copy any paste any code that
> you've already written, or that you downloaded from my finished versions. You
> just spend 3-weeks at this, don't cheat yourself! Go and watch the video, and
> then code it yourself. Reinforce what you learned by writing it out again!

- [Oswald font](https://fonts.google.com/specimen/Oswald?category=Sans+Serif,Display,Handwriting,Monospace&query=Oswa).

- [Final Challenge!!](https://gitlab.com/rbprogrammer-courseware/conquering_responsive_layouts/-/tree/master/challenges/final-challenge)
